package core.data;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import java.util.List;
import javax.persistence.*;
@Entity
@Table(name = "apex_data")
public class ApexData {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private int id;
    @OneToOne(cascade = {CascadeType.ALL})
    private AnonymousClass data;

    public AnonymousClass getData() {
        return data;
    }

    public void setData(AnonymousClass data) {
        this.data = data;
    }
}
@Entity
@Table(name = "platform_info")
class PlatformInfo {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private int id;

    @Column(name = "platform_user_id", unique = false)
    private String platformUserId;

    public String getPlatformUserId() {
        return platformUserId;
    }

    public void setPlatformUserId(String platformUserId) {
        this.platformUserId = platformUserId;
    }
}


