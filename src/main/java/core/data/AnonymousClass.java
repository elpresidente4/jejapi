package core.data;

import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.List;
@Service
@Entity
@Table(name = "anonymous_class")
public class AnonymousClass {
    @Id
    @Column(name = "id", unique = true)
    @GeneratedValue(strategy=GenerationType.IDENTITY)

    private int id;


    @OneToOne(cascade = {CascadeType.ALL})
    private PlatformInfo platformInfo;

    @OneToMany(targetEntity = Segments.class, mappedBy = "id")

    private List<Segments> segments;

    public List<Segments> getSegments() {
        return segments;
    }

    public void setSegments(List<Segments> segments) {
        this.segments = segments;
    }

    public PlatformInfo getPlatformInfo() {
        return platformInfo;
    }

    public void setPlatformInfo(PlatformInfo platformInfo) {
        this.platformInfo = platformInfo;
    }
}