package core.api;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import core.data.AnonymousClass;
import core.data.ApexData;
import core.data.Segments;
import core.storage.ApexDao;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ApexApiController {
    private OkHttpClient client = new OkHttpClient();
    private Map<String, Object> cacheData = new HashMap();
    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private ApexDao apexDao;
    @Autowired
    private AnonymousClass anonymousClass;

    public ApexApiController() {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @GetMapping(value = "/myData")
    public List<ApexData> mesData() {
        return (List<ApexData>) apexDao.getMyData();

    }
    @PostMapping(value = "/createData")
    public void createData() {
        ApexData apexData = new ApexData();
        apexData.setData(anonymousClass);
        apexDao.putData(apexData);
    }

    @GetMapping(value="/playerData/{playerName}")
    public Object getPlayerInfo(@PathVariable String playerName) throws IOException {
        if ( cacheData.get(playerName) != null) {
            return cacheData.get(playerName);
        }
        Request request = new Request.Builder()
                .header("TRN-Api-Key", "aa9924f9-308a-4baa-b8b9-a3fa522900de")
                .url("https://public-api.tracker.gg/v2/apex/standard/profile/origin/" + playerName)
                .build();
        Response response = client.newCall(request).execute();
        ApexData mappedApexData = objectMapper.readValue(response.body().string(), ApexData.class);
        cacheData.put(playerName, mappedApexData);
        apexDao.putData(mappedApexData);
        return mappedApexData;
    }
    public ApexDao getApexDao() {
        return apexDao;
    }

    public void setApexDao(ApexDao apexDao) {
        this.apexDao = apexDao;
    }
}
