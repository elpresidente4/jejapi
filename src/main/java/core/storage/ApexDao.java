package core.storage;

import core.data.ApexData;
import core.data.Segments;
import org.springframework.stereotype.Service;

@Service
public interface ApexDao {
    Iterable<ApexData> getMyData();
    void putData(ApexData apexData);
}
