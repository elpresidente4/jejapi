package core.storage;

import core.data.ApexData;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;

@Service
@ConditionalOnProperty(name = "storagemode", havingValue = "database")
public class ApexDaoDatabase implements ApexDao {

    @Value("${connectionURL}")
    private String connectionURL;

    @Autowired
    private ApexRepository apexRepository;

    public Iterable<ApexData> getMyData() {
        return apexRepository.findAll();
    }

    @Override
    public void putData(ApexData apexData) {
        apexRepository.save(apexData);
    }

    public String getConnectionURL() {
        return connectionURL;
    }

    public void setConnectionURL(String connectionURL) {
        this.connectionURL = connectionURL;
    }
}
