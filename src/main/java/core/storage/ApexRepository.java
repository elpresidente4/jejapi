package core.storage;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import core.data.ApexData;

@Repository
public interface ApexRepository extends CrudRepository<ApexData, Integer> {
}

