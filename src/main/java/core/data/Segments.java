package core.data;

import javax.persistence.*;

@Entity
@Table(name = "segments")
public class Segments {
    @Id
    @Column(name = "id", unique = true)
    private int id ;

    @OneToOne(cascade = {CascadeType.ALL})
    private Metadata metadata;
    @OneToOne(cascade = {CascadeType.ALL})
    private Stats stats;

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

}
@Entity
class Metadata {
    @Id
    @Column(name = "id", unique = true)
    private int id;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
@Entity
class Stats {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private int id;

    @OneToOne(cascade = {CascadeType.ALL})
    private Kills kills;

    public Kills getKills() {
        return kills;
    }

    public void setKills(Kills kills) {
        this.kills = kills;
    }
}
@Entity
class Kills {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private int id;

    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
